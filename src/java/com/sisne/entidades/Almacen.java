/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sisne.entidades;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author LINA_CDT
 */
@Entity
@Table(name = "almacen")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Almacen.findAll", query = "SELECT a FROM Almacen a")
    , @NamedQuery(name = "Almacen.findByIdAlmacen", query = "SELECT a FROM Almacen a WHERE a.idAlmacen = :idAlmacen")
    , @NamedQuery(name = "Almacen.findByFechaRegistro", query = "SELECT a FROM Almacen a WHERE a.fechaRegistro = :fechaRegistro")
    , @NamedQuery(name = "Almacen.findByCantidad", query = "SELECT a FROM Almacen a WHERE a.cantidad = :cantidad")
    , @NamedQuery(name = "Almacen.findByIva", query = "SELECT a FROM Almacen a WHERE a.iva = :iva")
    , @NamedQuery(name = "Almacen.findByFechaPreacion", query = "SELECT a FROM Almacen a WHERE a.fechaPreacion = :fechaPreacion")
    , @NamedQuery(name = "Almacen.findByEstado", query = "SELECT a FROM Almacen a WHERE a.estado = :estado")})
public class Almacen implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "id_almacen")
    private Integer idAlmacen;
    @Column(name = "fecha_registro")
    @Temporal(TemporalType.DATE)
    private Date fechaRegistro;
    @Column(name = "cantidad")
    private Integer cantidad;
    @Column(name = "iva")
    private BigInteger iva;
    @Column(name = "fecha_preacion")
    @Temporal(TemporalType.DATE)
    private Date fechaPreacion;
    @Column(name = "estado")
    private Boolean estado;
    @JoinColumn(name = "fk_bodega", referencedColumnName = "id_bodega")
    @ManyToOne
    private Bodega fkBodega;
    @JoinColumn(name = "fk_producto", referencedColumnName = "id_producto")
    @ManyToOne
    private Producto fkProducto;
    @JoinColumn(name = "fk_proveedor", referencedColumnName = "id_proveedor")
    @ManyToOne
    private Proveedor fkProveedor;

    public Almacen() {
    }

    public Almacen(Integer idAlmacen) {
        this.idAlmacen = idAlmacen;
    }

    public Integer getIdAlmacen() {
        return idAlmacen;
    }

    public void setIdAlmacen(Integer idAlmacen) {
        this.idAlmacen = idAlmacen;
    }

    public Date getFechaRegistro() {
        return fechaRegistro;
    }

    public void setFechaRegistro(Date fechaRegistro) {
        this.fechaRegistro = fechaRegistro;
    }

    public Integer getCantidad() {
        return cantidad;
    }

    public void setCantidad(Integer cantidad) {
        this.cantidad = cantidad;
    }

    public BigInteger getIva() {
        return iva;
    }

    public void setIva(BigInteger iva) {
        this.iva = iva;
    }

    public Date getFechaPreacion() {
        return fechaPreacion;
    }

    public void setFechaPreacion(Date fechaPreacion) {
        this.fechaPreacion = fechaPreacion;
    }

    public Boolean getEstado() {
        return estado;
    }

    public void setEstado(Boolean estado) {
        this.estado = estado;
    }

    public Bodega getFkBodega() {
        return fkBodega;
    }

    public void setFkBodega(Bodega fkBodega) {
        this.fkBodega = fkBodega;
    }

    public Producto getFkProducto() {
        return fkProducto;
    }

    public void setFkProducto(Producto fkProducto) {
        this.fkProducto = fkProducto;
    }

    public Proveedor getFkProveedor() {
        return fkProveedor;
    }

    public void setFkProveedor(Proveedor fkProveedor) {
        this.fkProveedor = fkProveedor;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idAlmacen != null ? idAlmacen.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Almacen)) {
            return false;
        }
        Almacen other = (Almacen) object;
        if ((this.idAlmacen == null && other.idAlmacen != null) || (this.idAlmacen != null && !this.idAlmacen.equals(other.idAlmacen))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.sisne.entidades.Almacen[ idAlmacen=" + idAlmacen + " ]";
    }
    
}
