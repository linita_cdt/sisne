/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sisne.entidades;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author LINA_CDT
 */
@Entity
@Table(name = "estadocliente")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Estadocliente.findAll", query = "SELECT e FROM Estadocliente e")
    , @NamedQuery(name = "Estadocliente.findByNombre", query = "SELECT e FROM Estadocliente e WHERE e.nombre = :nombre")
    , @NamedQuery(name = "Estadocliente.findByEstado", query = "SELECT e FROM Estadocliente e WHERE e.estado = :estado")
    , @NamedQuery(name = "Estadocliente.findByIdEstadoCliente", query = "SELECT e FROM Estadocliente e WHERE e.idEstadoCliente = :idEstadoCliente")})
public class Estadocliente implements Serializable {

    private static final long serialVersionUID = 1L;
    @Size(max = 100)
    @Column(name = "nombre")
    private String nombre;
    @Column(name = "estado")
    private Boolean estado;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "id_estado_cliente")
    private Integer idEstadoCliente;
    @OneToMany(mappedBy = "fkEstadoCliente")
    private List<Cliente> clienteList;

    public Estadocliente() {
    }

    public Estadocliente(Integer idEstadoCliente) {
        this.idEstadoCliente = idEstadoCliente;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Boolean getEstado() {
        return estado;
    }

    public void setEstado(Boolean estado) {
        this.estado = estado;
    }

    public Integer getIdEstadoCliente() {
        return idEstadoCliente;
    }

    public void setIdEstadoCliente(Integer idEstadoCliente) {
        this.idEstadoCliente = idEstadoCliente;
    }

    @XmlTransient
    public List<Cliente> getClienteList() {
        return clienteList;
    }

    public void setClienteList(List<Cliente> clienteList) {
        this.clienteList = clienteList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idEstadoCliente != null ? idEstadoCliente.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Estadocliente)) {
            return false;
        }
        Estadocliente other = (Estadocliente) object;
        if ((this.idEstadoCliente == null && other.idEstadoCliente != null) || (this.idEstadoCliente != null && !this.idEstadoCliente.equals(other.idEstadoCliente))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.sisne.entidades.Estadocliente[ idEstadoCliente=" + idEstadoCliente + " ]";
    }
    
}
