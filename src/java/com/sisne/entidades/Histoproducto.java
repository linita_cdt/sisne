/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sisne.entidades;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author LINA_CDT
 */
@Entity
@Table(name = "histoproducto")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Histoproducto.findAll", query = "SELECT h FROM Histoproducto h")
    , @NamedQuery(name = "Histoproducto.findByIdProducto", query = "SELECT h FROM Histoproducto h WHERE h.idProducto = :idProducto")
    , @NamedQuery(name = "Histoproducto.findByDescripcion", query = "SELECT h FROM Histoproducto h WHERE h.descripcion = :descripcion")
    , @NamedQuery(name = "Histoproducto.findByPrecioVenta", query = "SELECT h FROM Histoproducto h WHERE h.precioVenta = :precioVenta")
    , @NamedQuery(name = "Histoproducto.findByPrecioCompra", query = "SELECT h FROM Histoproducto h WHERE h.precioCompra = :precioCompra")
    , @NamedQuery(name = "Histoproducto.findByIva", query = "SELECT h FROM Histoproducto h WHERE h.iva = :iva")
    , @NamedQuery(name = "Histoproducto.findByEstado", query = "SELECT h FROM Histoproducto h WHERE h.estado = :estado")
    , @NamedQuery(name = "Histoproducto.findByFechaCreacion", query = "SELECT h FROM Histoproducto h WHERE h.fechaCreacion = :fechaCreacion")})
public class Histoproducto implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "id_producto")
    private Integer idProducto;
    @Size(max = 100)
    @Column(name = "descripcion")
    private String descripcion;
    @Column(name = "precio_venta")
    private BigInteger precioVenta;
    @Column(name = "precio_compra")
    private BigInteger precioCompra;
    @Column(name = "iva")
    private BigInteger iva;
    @Column(name = "estado")
    private Boolean estado;
    @Column(name = "fecha_creacion")
    @Temporal(TemporalType.DATE)
    private Date fechaCreacion;
    @JoinColumn(name = "fk_proveedor", referencedColumnName = "id_proveedor")
    @ManyToOne
    private Proveedor fkProveedor;
    @JoinColumn(name = "fk_tipo_producto", referencedColumnName = "id_tipo_producto")
    @ManyToOne
    private Tipoproducto fkTipoProducto;

    public Histoproducto() {
    }

    public Histoproducto(Integer idProducto) {
        this.idProducto = idProducto;
    }

    public Integer getIdProducto() {
        return idProducto;
    }

    public void setIdProducto(Integer idProducto) {
        this.idProducto = idProducto;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public BigInteger getPrecioVenta() {
        return precioVenta;
    }

    public void setPrecioVenta(BigInteger precioVenta) {
        this.precioVenta = precioVenta;
    }

    public BigInteger getPrecioCompra() {
        return precioCompra;
    }

    public void setPrecioCompra(BigInteger precioCompra) {
        this.precioCompra = precioCompra;
    }

    public BigInteger getIva() {
        return iva;
    }

    public void setIva(BigInteger iva) {
        this.iva = iva;
    }

    public Boolean getEstado() {
        return estado;
    }

    public void setEstado(Boolean estado) {
        this.estado = estado;
    }

    public Date getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public Proveedor getFkProveedor() {
        return fkProveedor;
    }

    public void setFkProveedor(Proveedor fkProveedor) {
        this.fkProveedor = fkProveedor;
    }

    public Tipoproducto getFkTipoProducto() {
        return fkTipoProducto;
    }

    public void setFkTipoProducto(Tipoproducto fkTipoProducto) {
        this.fkTipoProducto = fkTipoProducto;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idProducto != null ? idProducto.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Histoproducto)) {
            return false;
        }
        Histoproducto other = (Histoproducto) object;
        if ((this.idProducto == null && other.idProducto != null) || (this.idProducto != null && !this.idProducto.equals(other.idProducto))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.sisne.entidades.Histoproducto[ idProducto=" + idProducto + " ]";
    }
    
}
