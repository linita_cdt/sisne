/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sisne.controlador;

import com.sisne.entidades.Usuario;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author LINA_CDT
 */
@Stateless
public class UsuarioFacade extends AbstractFacade<Usuario> {

    @PersistenceContext(unitName = "sisnePU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public UsuarioFacade() {
        super(Usuario.class);
    }
      public Usuario obtenerxTipoUsuario(String correoUser){
        try {
            Query q= em.createNamedQuery("Usuario.findByEmail", Usuario.class).setParameter("email", correoUser);
            Usuario user = (Usuario) q.getSingleResult();
            return user;
        } catch (NoResultException e) {
            return null;
        }
}
}