/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sisne.controlador;

import com.sisne.entidades.Estadocliente;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author LINA_CDT
 */
@Stateless
public class EstadoclienteFacade extends AbstractFacade<Estadocliente> {

    @PersistenceContext(unitName = "sisnePU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public EstadoclienteFacade() {
        super(Estadocliente.class);
    }
    
}
