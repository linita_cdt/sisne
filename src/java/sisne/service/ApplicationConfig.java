/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sisne.service;

import java.util.Set;
import javax.ws.rs.core.Application;

/**
 *
 * @author LINA_CDT
 */
@javax.ws.rs.ApplicationPath("webresources")
public class ApplicationConfig extends Application {

    @Override
    public Set<Class<?>> getClasses() {
        Set<Class<?>> resources = new java.util.HashSet<>();
        addRestResourceClasses(resources);
        return resources;
    }

    /**
     * Do not modify addRestResourceClasses() method.
     * It is automatically populated with
     * all resources defined in the project.
     * If required, comment out calling this method in getClasses().
     */
    private void addRestResourceClasses(Set<Class<?>> resources) {
        resources.add(sisne.service.AlmacenFacadeREST.class);
        resources.add(sisne.service.BodegaFacadeREST.class);
        resources.add(sisne.service.ClienteFacadeREST.class);
        resources.add(sisne.service.EstadoclienteFacadeREST.class);
        resources.add(sisne.service.EstadofacturaFacadeREST.class);
        resources.add(sisne.service.FacturaFacadeREST.class);
        resources.add(sisne.service.HistoproductoFacadeREST.class);
        resources.add(sisne.service.PermisoFacadeREST.class);
        resources.add(sisne.service.ProductoFacadeREST.class);
        resources.add(sisne.service.ProveedorFacadeREST.class);
        resources.add(sisne.service.RolFacadeREST.class);
        resources.add(sisne.service.TipoproductoFacadeREST.class);
        resources.add(sisne.service.UsuarioFacadeREST.class);
    }
    
}
